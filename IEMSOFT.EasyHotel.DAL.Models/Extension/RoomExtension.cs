﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.DAL.Models;
namespace IEMSOFT.EasyHotel.DAL.Models.Extension
{
  public  class RoomExtension:Room
    {
        public string RoomTypeName { get; set; }
        public string SubHotelName { get; set; }
        public string SubHotelAddress { get; set; }
        public string SubHotelPhone { get; set; }
        public string SubHotelFax { get; set; }
    }
}
