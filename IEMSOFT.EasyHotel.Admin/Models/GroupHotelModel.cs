﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class GroupHotelModel
    {
        public int GroupHotelId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}