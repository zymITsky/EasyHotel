﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public int GroupHotelId { get; set; }
    }
}